/**
  ******************************************************************************
  * @file    main.c
  * @author  fire
  * @version V1.0
  * @date    2013-xx-xx
  * @brief   RCC-时钟配置
  ******************************************************************************
  * @attention
  *
  * 实验平台:野火 F103 指南者 STM32 开发板 
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :https://fire-stm32.taobao.com
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx.h"
#include "./led/bsp_led.h"
#include "./rcc/bsp_clkconfig.h"

void Delay(__IO uint32_t nCount)	 //简单的延时函数
{
	for(; nCount != 0; nCount--);
}

/**
  * @brief  主函数
  * @param  无
  * @retval 无
  */
int main(void)
{
    // 使用HSE，配置系统时钟为72M
    HSE_SetSysClock();
	/* LED 端口初始化 */
	LED_GPIO_Config();	 
  
     while (1)
    {
        LED2( ON );			 // 亮 
        Delay(0x0FFFFF);
        LED2( OFF );		 // 灭
        Delay(0x0FFFFF);
    }
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
